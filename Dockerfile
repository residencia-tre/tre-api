FROM node:19-alpine
WORKDIR /usr/src/app
COPY . .
RUN npm install
EXPOSE 3030
ENTRYPOINT [ "npm" ]
CMD ["start"]