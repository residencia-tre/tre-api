const express = require('express')
const app = express()
const port = 3030

app.get('/', (req, res) => {
  res.send(`Hello World 3.0!! TRE_ENVDATA: ${process.env.TRE_ENVDATA}`)
})

app.get('/k8s', (req, res) => {
  res.send('Running on Kubernetes!!!')
})

app.get('/health', (req, res) => {
  if (Math.random() > 0.8)
    res.sendStatus(500)
  else
    res.send('OK')
})


app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
